var express = require('express');
var swig = require('swig');
var router = express.Router();
//Cliente Mongo
var MongoClient = require('mongodb').MongoClient;

//Crypto
const crypto = require('crypto');
const secreto = 'abcdefg';

// Session
var expressSession = require('express-session');

router.use(expressSession({
    secret: 'abcdefg',
    resave: true,
    saveUninitialized: true
}));

// Router - zonaprivada
var zonaprivada = express.Router();

zonaprivada.use(function (req, res, next) {

    if (req.session.usuario) {
        // dejamos correr la petición
        console.log("dejamos correr la petición")
        next();
    } else {
        req.session.destino = req.originalUrl;
        console.log("va a : " + req.session.destino)
        res.redirect("/login");
    }

});

// Aplicar zonaprivada a las siguientes URLs
router.use("/anuncios/publicar", zonaprivada);
router.use("/anuncios/eliminar", zonaprivada);
router.use("/anuncios/modificar", zonaprivada);
router.use("/anuncios/misanuncios", zonaprivada);


/* GET home page. Redirigimos a la página de login*/
router.get('/', function (req, res, next) {
    res.redirect("/login");
});

router.post('/anuncios/publicarSinFoto', function (req, res) {
    var nuevoAnuncio = {
        titulo: req.body.titulo,
        categoria: req.body.categoria,
        descripcion: req.body.descripcion,
        usuario: req.session.usuario
    }

    // Abrir el cliente
    MongoClient.connect('mongodb://localhost:27017/anuncios',
        function (err, db) {
            if (err) {
                var respuesta = swig.renderFile('views/mensaje.html', {
                    mensaje: "Ha ocurrido un problema al publicar el anuncio"
                });
                res.send(respuesta);

            } else {
                console.log("Conectado al servidor");

                var collection = db.collection('anuncios');
                collection.insert(nuevoAnuncio,
                    function (err, result) {
                        if (err) {
                            var respuesta = swig.renderFile('views/mensaje.html', {
                                mensaje: "Ha ocurrido un problema al publicar el anuncio"
                            });
                            res.send(respuesta);

                        } else {
                            var respuesta = swig.renderFile('views/mensaje.html', {
                                mensaje: "Su anuncio " + result.ops[0].titulo +
                                    " ha sido publicado con éxito. Código: "
                                    + result.ops[0]._id
                            });
                            res.send(respuesta);
                        }
                        db.close();
                    });
            }
        });
})

router.post('/anuncios/publicar', function (req, res) {

    var nuevoAnuncio = {
        titulo: req.body.titulo,
        categoria: req.body.categoria,
        descripcion: req.body.descripcion,
        usuario: req.session.usuario
    }

    // Abrir el cliente
    MongoClient.connect('mongodb://localhost:27017/anuncios',
        function (err, db) {
            if (err) {
                var respuesta = swig.renderFile('views/mensaje.html', {
                    mensaje: "Ha ocurrido un problema al publicar el anucio"
                });
                res.send(respuesta);

            } else {
                console.log("Conectado al servidor");

                var collection = db.collection('anuncios');
                collection.insert(nuevoAnuncio,
                    function (err, result) {
                        if (err) {
                            var respuesta = swig.renderFile('views/mensaje.html', {
                                mensaje: "Ha ocurrido un problema al publicar el anucio"
                            });
                            res.send(respuesta);

                        } else {
                            var anuncioId = result.ops[0]._id;

                            if (req.files.imagen.name != "") {
                                var imagen = req.files.imagen;
                                imagen.mv('public/images/' + anuncioId + '-1.jpg',
                                    function (err) {
                                        if (err) {
                                            var respuesta = swig.renderFile('views/mensaje.html', {
                                                mensaje: "Ha ocurrido un problema con la imagen."
                                            });
                                            res.send(respuesta);
                                        } else {
                                            var respuesta = swig.renderFile('views/mensaje.html', {
                                                mensaje: "Su anuncio " + result.ops[0].titulo +
                                                    " ha sido publicado con éxito. Código: "
                                                    + result.ops[0]._id
                                            });
                                            res.send(respuesta);
                                        }
                                    });
                            }
                        }
                        db.close();
                    });
            }
        });

})

router.get('/anuncios/categoria/:nombre', function (req, res) {
    // Abrir el cliente
    MongoClient.connect('mongodb://localhost:27017/anuncios',
        function (err, db) {
            console.log("Error:  " + err)
            if (err) {
                res.send("Error de conexión: " + err);
            } else {
                console.log("Conectado al servidor")
                var collection = db.collection('anuncios');

                collection.find({categoria: req.params.nombre})
                    .toArray(function (err, anuncios) {

                        db.close();

                        var respuesta = swig.renderFile('views/categoria.html', {
                            nombre_categoria: req.params.nombre,
                            anuncios: anuncios
                        });

                        res.send(respuesta);
                    });
            }
        });
})

router.get('/anuncio/:id', function (req, res) {
    // Abrir el cliente
    MongoClient.connect('mongodb://localhost:27017/anuncios',
        function (err, db) {
            console.log("Error:  " + err)
            if (err) {
                res.send("Error de conexión: " + err);
            } else {
                console.log("Conectado al servidor")
                var collection = db.collection('anuncios');
                var idAnuncio = require('mongodb').ObjectID(req.params.id);

                collection.find({_id: idAnuncio}).toArray(function (err, anuncios) {
                    db.close();

                    var respuesta = swig.renderFile('views/anuncio.html', {
                        anuncio: anuncios[0]
                    });

                    res.send(respuesta);
                });
            }
        });
})

router.get('/anuncios/publicar', function (req, res) {
    var respuesta = swig.renderFile('views/publicar.html', {
        usuario: req.session.usuario
    });
    res.send(respuesta);

})

router.get('/anuncios/publicar', function (req, res) {
    var respuesta = swig.renderFile('views/publicar.html', {
        usuario: req.session.usuario
    });
    res.send(respuesta);
})

//Registro de usuario
router.get('/registro', function (req, res) {
    var respuesta = swig.renderFile('views/registro.html', {});
    res.send(respuesta);
})

//Usuario
router.post('/usuario', function (req, res) {
    var passwordSeguro = crypto.createHmac('sha256', secreto)
        .update(req.body.password).digest('hex');

    var nuevoUsuario = {
        nombre: req.body.nombre,
        email: req.body.email,
        password: passwordSeguro
    }

    // Abrir el cliente
    MongoClient.connect('mongodb://localhost:27017/anuncios',
        function (err, db) {
            if (err) {
                res.send("Error de conexión: " + err);
            } else {
                console.log("Conectado al servidor");
                var collection = db.collection('usuarios');

                collection.insert(nuevoUsuario,
                    function (err, result) {
                        if (err) {
                            res.send("Error al insertar " + err);
                        } else {
                            res.send('Usuario Insertado ' + result);
                        }
                        // Cerrar el cliente
                        db.close();
                    });
            }
        });
})

//Login
router.get('/login', function (req, res) {
    var respuesta = swig.renderFile('views/login.html', {
        error: req.query.error
    });
    res.send(respuesta);
})

//Login
router.post('/login', function (req, res) {
    var passwordSeguro = crypto.createHmac('sha256', secreto)
        .update(req.body.password).digest('hex');
    // Abrir el cliente
    MongoClient.connect('mongodb://localhost:27017/anuncios',
        function (err, db) {
            if (err) {
                console.log("error", err);
                // Error
                // Para redirect un post es post/login ...
                res.redirect("/login?error=true");
            } else {
                var collection = db.collection('usuarios');
                collection.find({email: req.body.email, password: passwordSeguro})
                    .toArray(function (err, usuarios) {

                        if (err || usuarios.length == 0) {
                            // No encontrado
                        } else {
                            // Encontrado
                            req.session.usuario = usuarios[0]._id.toString();
                            if (req.session.destino != null) {
                                res.redirect(req.session.destino);
                            } else {
                                res.redirect("/anuncios/publicar");
                            }
                        }
                        db.close();
                    });
            }
        });
})

router.get('/logout', function (req, res) {
    req.session.usuario = null;
    res.redirect("/");
})

router.get('/anuncios/misanuncios', function (req, res) {
    // Abrir el cliente
    MongoClient.connect('mongodb://localhost:27017/anuncios',
        function (err, db) {
            console.log("Error:  " + err)
            if (err) {
                res.send("Error de conexión: " + err);
            } else {
                console.log("Conectado al servidor")

                var collection = db.collection('anuncios');
                collection.count(function (err, count) {
                    collection.find({usuario: req.session.usuario})
                        .toArray(function (err, anuncios) {
                            db.close();

                            var respuesta = swig.renderFile('views/misanuncios.html', {
                                anuncios: anuncios,
                                cantidad: count,
                                usuario: req.session.usuario
                            });
                            res.send(respuesta);
                        });
                });
            }
        });
})

router.get('/anuncios/modificar/:id', function (req, res) {
    // Abrir el cliente
    MongoClient.connect('mongodb://localhost:27017/anuncios',
        function (err, db) {
            console.log("Error:  " + err)
            if (err) {
                res.send("Error de conexión: " + err);
            } else {
                console.log("Conectado al servidor")
                var collection = db.collection('anuncios');
                var idAnuncio = require('mongodb').ObjectID(req.params.id);

                collection.find({_id: idAnuncio}).toArray(function (err, anuncios) {
                    db.close();
                    var respuesta = swig.renderFile('views/modificar.html', {
                        anuncio: anuncios[0]
                    });
                    res.send(respuesta);
                });
            }
        });
})

router.post('/anuncios/modificar/:id', function (req, res) {
    // Datos a modificar
    var nuevosDatos = {
        titulo: req.body.titulo,
        categoria: req.body.categoria,
        descripcion: req.body.descripcion
    }

    // Abrir el cliente
    MongoClient.connect('mongodb://localhost:27017/anuncios',
        function (err, db) {
            if (err) {
                res.send("Error de conexión: " + err);
            } else {
                console.log("Conectado al servidor");
                var collection = db.collection('anuncios');
                // Transformar a Mongo ObjectID
                var id = require('mongodb').ObjectID(req.params.id);

                collection.update({_id: id}, {$set: nuevosDatos},
                    function (err, result) {
                        if (err) {
                            var respuesta = swig.renderFile('views/mensaje.html', {
                                mensaje: "Problema al modificar el anuncio"
                            });
                            res.send(respuesta);
                        } else {
                            // No Tiene imagen -> guardamos y respondemos
                            if ( req.files != null && req.files.imagen.name != "") {
                                var imagen = req.files.imagen;
                                imagen.mv('public/images/' + req.params.id + '-1.jpg',
                                    function (err) {
                                        if (err) {
                                            var respuesta =
                                                swig.renderFile('views/mensaje.html', {
                                                    mensaje: "Problema con la imagen."
                                                });
                                            res.send(respuesta);
                                        } else {
                                            var respuesta =
                                                swig.renderFile('views/mensaje.html', {
                                                    mensaje: "Su anuncio " + req.body.titulo +
                                                        " ha sido modificado."
                                                });
                                            res.send(respuesta);
                                        }
                                    });
                                // No tiene imagen -> respondemos
                            } else {
                                var respuesta =
                                    swig.renderFile('views/mensaje.html', {
                                        mensaje: "Su anuncio " + req.body.titulo +
                                            " ha sido modificado."
                                    });
                                res.send(respuesta);
                            }
                        }
                        db.close();
                    });
            }
        });
})

router.get('/anuncios/eliminar/:id', function (req, res) {
    // Abrir el cliente
    MongoClient.connect('mongodb://localhost:27017/anuncios',
        function (err, db) {
            if (err) {
                res.send("Error de conexión: " + err);
            } else {
                console.log("Conectado al servidor");
                var collection = db.collection('anuncios');

                // Transformar a Mongo ObjectID
                var id = require('mongodb').ObjectID(req.params.id);

                collection.remove({_id: id}, function (err, result) {
                    if (err) {
                        var respuesta = swig.renderFile('views/mensaje.html', {
                            mensaje: "Problema al Eliminar el anuncio"
                        });
                        res.send(respuesta);
                    } else {
                        res.redirect("/anuncios/misanuncios");
                    }
                    db.close();
                });
            }
        });
})

router.get('/api/anuncios/categoria/:nombre', function (req, res) {
    // Abrir el cliente
    MongoClient.connect('mongodb://localhost:27017/anuncios',
        function (err, db) {
            console.log("Error:  " + err)
            if (err) {
                res.send("Error de conexión: " + err);
            } else {
                console.log("Conectado al servidor")
                var collection = db.collection('anuncios');

                collection.count(function (err, count) {

                    collection.find({categoria: req.params.nombre})
                        .toArray(function (err, anuncios) {
                            db.close();
                            res.send(JSON.stringify(anuncios));
                        });
                });
            }
        });
})


module.exports = router;
