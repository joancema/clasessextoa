var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var bodyParser = require('body-parser')

var cors = require('cors');

// fileupload
var fileUpload = require('express-fileupload');

var categorias = ["",""];

var app = express();


app.use(fileUpload());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'twig');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/', indexRouter);
app.use('/users', usersRouter);


// CORS
app.use(cors());

app.use(express.static(path.join(__dirname, 'public')));

app.use('/views', express.static(path.resolve(__dirname, 'views')));
app.use('/public', express.static(path.resolve(__dirname, 'public')));


//Estáticos anuncios
app.use('/anuncios/public/javascripts',express.static(path.join(__dirname, 'public/javascripts')));
app.use('/anuncios/public/stylesheets',express.static(path.join(__dirname, 'public/stylesheets')));
app.use('/anuncios/public/jquery',express.static(path.join(__dirname, 'public/jquery')));
app.use('/anuncios/public/fonts',express.static(path.join(__dirname, 'public/fonts')));

//Estáticos anuncios/categoria
app.use('/anuncios/categoria/public/javascripts',express.static(path.join(__dirname, 'public/javascripts')));
app.use('/anuncios/categoria/public/stylesheets',express.static(path.join(__dirname, 'public/stylesheets')));
app.use('/anuncios/categoria/public/jquery',express.static(path.join(__dirname, 'public/jquery')));
app.use('/anuncios/categoria/public/fonts',express.static(path.join(__dirname, 'public/fonts')));

//Estáticos anuncios/modificar
app.use('/anuncios/modificar/public/javascripts',express.static(path.join(__dirname, 'public/javascripts')));
app.use('/anuncios/modificar/public/stylesheets',express.static(path.join(__dirname, 'public/stylesheets')));
app.use('/anuncios/modificar/public/jquery',express.static(path.join(__dirname, 'public/jquery')));
app.use('/anuncios/modificar/public/fonts',express.static(path.join(__dirname, 'public/fonts')));



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});


// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.listen(8080, function () {
  console.log('Servidor listo');
});


module.exports = app;
