let mongoose = require('mongoose');
let Schema = mongoose.Schema;


let UserSchema = new Schema({
    name: { type: String, required: true },
    username: { type: String, required: true },
    password: { type: String, required: true }
  });
  

let Usuario = mongoose.model('Usuarios', UserSchema);

module.exports = Usuario;