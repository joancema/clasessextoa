const fs = require('fs')
const http = require('http')
const texto = require('./libreria').joancema()

const scriptServer = fs.readFileSync('./funciones.js');


http.createServer((request, response)=>{
    const {url} = request;
    if (url==='/')
    {
        response.writeHead(200,{"Content-type":"text/html"});
        console.log(`<script> ${scriptServer.toString() } </script> ${texto}`)
        response.write(`<script> ${scriptServer.toString() } </script> ${texto}` );
    }
    else
    {
        response.writeHead(404,{"Content-type":"text/html"});
        response.write("write not found");

    }
    response.end();
})
.listen(3000);

