const fs = require('fs');
const express = require('express');
const cors = require('cors')

const path = require('path')

const PUERTO=3000;
const index = fs.readFileSync('./index.html');
const about = fs.readFileSync('./about.html');
/// iniciar un nuevo servidor
const server = express();


//definir la ruta del proyecto

var paginaDeError =  path.join( __dirname, "./error.html"  )


//utilizando una funciOn normal y llamAndola en lInea
server.get("/", devolverIndex )


//utilizando una funciOn flecha 
server.get("/about", (req,res)=>{
    res.write(about)
})

//anteriormente bodyparser, ahora viene en el CORE de NODE
//Middleware son hooks mAs completos.
server.use(express.json()).use(cors())
//cors 


server.listen(PUERTO, ()=>{
    console.log(`Servidor esta ejecutandose por el puerto ${PUERTO}`)
})

//definiendo un Middleware muy bAsico
server.use((req,res,next)=>{
    res.status(404).sendFile( paginaDeError )
    //next();

})


//paso 1
//paso 2
//paso 3
// middleware  req, res, next()
//paso 4
//paso 5



function devolverIndex(req,res)
{
    res.write(index)
}


/*
const fs = require('fs')
const http = require('http')


const index = fs.readFileSync('./index.html');
const about = fs.readFileSync('./about.html');



http.createServer((request,response)=>{
    const { url } = request;
    if (url==='/')
    {
        response.writeHead(200, {"Content-type":"text/html"});
        response.write(index);

    }
    else if (url==='/about')
    {
        response.writeHead(200, {"Content-type":"text/html"});
        response.write(about);

    }
    else
    {
        response.writeHead(404, {"Content-type":"text/html"});
        response.write("Not found!!");
    }

})
.listen(3000, ()=>{
    console.log('Servidor corriendo')
})


*/