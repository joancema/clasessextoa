const express = require('express');
const app = express();
const PORT= 3000

//recurso 
let arreglo=[];

app.use('/public', express.static(__dirname+'/public'))
app.use(express.json())

//GET con todos los elementos
app.get('/', (req, res)=>{
    res.send(arreglo)
})

// GET individual
// localhost:3000/individual/pegazo
app.get('/individual/:nombrex',(req,res)=>{
    res.send( arreglo.filter(p=>{   return  p.nombre ===  req.params.nombrex;  })[0]   )
})

//POST para agregar un recurso
app.post('/',(req,res)=>{
     arreglo.push(req.body)
     res.send({"Mensaje":"La mascota fue almacenada sin problemas!!!"}) 
})

//modificar un recurso
/*
{
"nombre":"pegazo",
"raza":"de la calle",
"color":"negro"
}
*/
app.put('/',(req,res)=>{
    //primero filtrar el elemento que queremos editar
    let auxiliar= req.body;
    filtrado = arreglo.filter(p=>{   return  p.nombre ===  req.body.nombre;  })[0]
    //luego modificar los atributos 
    filtrado.raza=req.body.raza;
    filtrado.color= req.body.color;
    res.send({"Mensaje":"La mascota fue editada sin problemas!!!!"})
})

//eliminar un recurso
// localhost:3000/pegazo2
/*
pegazo1
pegazo2
pegazo3
*/
app.delete('/:nombrex',(req,res)=>{
    arreglo= arreglo.filter(p=>{   return  p.nombre !==  req.params.nombrex } ) ;
    res.send({"Mensaje":"El dato fue eliminado sin problemas!!!"})
})

app.listen(PORT, ()=>{
    console.log(`El servidor esta escuchando por el puerto ${PORT}`);
});
