

const saludar = (nombre) => {
    console.log(`Hola ${nombre}`);
}
saludar('John')


//Funciones flechas más prácticas para ser definidas, más elegantes
//más utilizadas de forma anonima, en especial para trabajar con colecciones
// Las funciones arrow no tienen contexto, por aquello no existe THIS, es decir
//su contexto es según quien la invoca
// las funciones flecha son siempre definidas como expresiones, es decir
// deben definirse previo a su uso.



/*


const saludar = function(nombre){
    console.log(`Hola ${nombre}`);
}

*/

/*
function saludar(parametro)
{
    console.log(`Hola ${parametro}`);
}

function saludarEnElBarrio(parametro)
{
    console.log(`Hola man como te va ${parametro}`);
}

function saludarBarcelonista(parametro)
{
    console.log(`Hola ${parametro} esta bonito ese reloj`);
}



function llamarFuncionParaSaludar(fn, parametro)
{
    fn(parametro)
}

//callbacks
//promises
//async/await


llamarFuncionParaSaludar(saludarBarcelonista  ,  'John');

*/
/*
class Persona
{
    constructor(nombre,apellido)
    {
        this.nombre=nombre;
        this.apellido=apellido;
    }
}

let john = new Persona('John','Cevallos');

console.log(john.nombre);

*/

//JSON
/*
let variable =
{
    nombre:'JOhn',
    apellido:'Cevallos',
    direccion:'Manta',
    telefono:'123123'
}

let propiedades= ['nombre','apellido','direccion','telefono']

//notaciOn de caja
for (let propiedad of propiedades)
{
    console.log(variable[propiedad]);
}

//notaciOn de punto

console.log(variable.nombre);

*/
