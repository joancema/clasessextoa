//Definir una persistencia (Arreglo, Base de datos, Servicio web monolítico, Micro)
const libros= [
    {
        id:1,
        titulo:'Aprendamos PHP como en 1990',
        idautor:2
    },
    {
        id:2,
        titulo:'Php para expertos',
        idautor:2
    },
    {
        id:3,
        titulo:'JavaScript para principiantes',
        idautor:1
    }
]

const autores = 
[
    {
        id:1,
        nombre:'Pedro Piguave'
    },
    {
        id:2,
        nombre:'Juan Carlos S'
    }
]


//funciOn que busca libros por ID
function buscarLibroPorId(id,callback)
{
    //buscamos en el arreglo libros uno que tenga ese ID
    const libro = libros.find( 
        (libro)=> 
        {
            return libro.id === id; 
        }
     )
    //si no existe generamos un error y lo enviamos al callback
     if ( !libro )
     {
         const error = new Error();
         error.message="El libro no existe!!"
        return callback(error);
     }
     //de no existir error debemos enviar el libro al callback
     callback(null, libro );
}

//funciOn que busca autores por ID
function buscarAutorPorId(id,callback)
{
    //buscamos en el arreglo autores uno que tenga ese ID
    const autor = autores.find( 
        (autor)=> 
        {
            return autor.id === id; 
        }
     )
    //si no existe generamos un error y lo enviamos al callback
     if ( !autor )
     {
         const error = new Error();
         error.message="El autor no existe!!"
        return callback(error);
     }
     //de no existir error debemos enviar el autor al callback
     callback(null, autor );
}



//CALLBACK HELL

//Invocamos un callback para tener el id del libro
buscarLibroPorId(3, (err,libro) => {
    if (err)
    {
        return console.log(err.message)
    }
    //invocamos otro callback para tener el autor
    buscarAutorPorId( libro.idautor , (err,autor)=>{
        if (err)
        {
            return console.log(err.message);
        }
        //Mostramos los datos del libro con el autor
        //console.log(` El libro ${libro.titulo} fue escrito por ${autor.nombre}` );
        libro.autor=autor
        delete libro.idautor;
        console.log(libro);
    } )

} )







