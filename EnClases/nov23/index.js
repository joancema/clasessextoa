//Definir una persistencia (Arreglo, Base de datos, Servicio web monolítico, Micro)
const libros= [
    {
        id:1,
        titulo:'Aprendamos PHP como en 1990',
        idautor:2
    },
    {
        id:2,
        titulo:'Php para expertos',
        idautor:2
    },
    {
        id:3,
        titulo:'JavaScript para principiantes',
        idautor:1
    }
]

const autores = 
[
    {
        id:1,
        nombre:'Pedro Piguave'
    },
    {
        id:2,
        nombre:'Juan Carlos S'
    }
]


//APLICAR promesas

//funciOn que busca libros por ID
async function buscarLibroPorId(id)
{
            
        //buscamos en el arreglo libros uno que tenga ese ID
        const libro = libros.find( 
            (libro)=> 
            {
                return libro.id === id; 
            }
         )
        //si no existe generamos un error y lo lanzamos
         if ( !libro )
         {
             const error = new Error();
             error.message="El libro no existe!!"
            throw error;
         }
         //de existir el libro  debemos retornarlo
         return libro;
}

//funciOn que busca autores por ID
async function buscarAutorPorId(id)
{
        //buscamos en el arreglo autores uno que tenga ese ID
        const autor = autores.find( 
            (autor)=> 
            {
                return autor.id === id; 
            }
        )
        //si no existe generamos un error lo lanzamos
        if ( !autor )
        {
            const error = new Error();
            error.message="El autor no existe!!"
            throw error;
        }
        //de existir debemos retornar el autor 
        return autor ;
    
}


async function mainx()
{
    try
    {
        //va a buscar un libro por el ID 3
        const libro = await buscarLibroPorId(3);
        //Luego que tengamos la respuesta vamos segUn el ID del libro encontrado a buscar su autor
        const autor = await buscarAutorPorId(libro.idautor);
        libro.autor= autor;
        delete libro.idautor;
        console.log(libro)
        //console.log(`El libro ${libro.titulo} tiene como autor a ${autor.nombre}` );
    }
    catch(ex)
    {
        console.log(ex.message);
    }

}



mainx();




