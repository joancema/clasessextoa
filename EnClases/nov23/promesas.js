//Definir una persistencia (Arreglo, Base de datos, Servicio web monolítico, Micro)
const libros= [
    {
        id:1,
        titulo:'Aprendamos PHP como en 1990',
        idautor:2
    },
    {
        id:2,
        titulo:'Php para expertos',
        idautor:2
    },
    {
        id:3,
        titulo:'JavaScript para principiantes',
        idautor:1
    }
]

const autores = 
[
    {
        id:1,
        nombre:'Pedro Piguave'
    },
    {
        id:2,
        nombre:'Juan Carlos S'
    }
]


//APLICAR promesas

//funciOn que busca libros por ID
function buscarLibroPorId(id)
{
    return new Promise((resolve, reject) =>{
        
        //buscamos en el arreglo libros uno que tenga ese ID
        const libro = libros.find( 
            (libro)=> 
            {
                return libro.id === id; 
            }
         )
        //si no existe generamos un error y rechazamos la promesa
         if ( !libro )
         {
             const error = new Error();
             error.message="El libro no existe!!"
            reject(error);
         }
         //de no existir error debemos enviar el libro y aceptar la promesa
         resolve(libro);
    })
}

//funciOn que busca autores por ID
function buscarAutorPorId(id)
{
    return new Promise((resolve,reject)=>{

        //buscamos en el arreglo autores uno que tenga ese ID
        const autor = autores.find( 
            (autor)=> 
            {
                return autor.id === id; 
            }
        )
        //si no existe generamos un error y lo enviamos al callback
        if ( !autor )
        {
            const error = new Error();
            error.message="El autor no existe!!"
            reject(error);
        }
        //de no existir error debemos enviar el autor al callback
        resolve( autor );
    })
    
}

let libroAuxiliar={};

buscarLibroPorId(30).then(libro=>{
    libroAuxiliar= libro;
    return buscarAutorPorId(libro.idautor);
}).then(autor=>{
    libroAuxiliar.autor= autor;
    delete libroAuxiliar.idautor;
    console.log(libroAuxiliar);
}).catch(err=>{
    console.log(err.message);
})

//retornar libroAuxiliar a nuestro usuario. Para que en el front-end 
//muestre la informaciOn en el formato que el prefiera 


