const express = require('express');
const app = express();


const response =
{
    data: [],
    services: "Monolithic service",
    architecture: "Monolithic"
}


//get      api/v1/
app.use((req,res,next)=>{
    response.data=[];
    next();
})


//get usuarios http://localhost:3000/api/v1/users
app.get('/api/v1/users', (req,res)=>{
    response.data.push('Administrador','Invitado', 'John');
    return res.send(response)
})

//get libros api/v1/books
app.get('/api/v1/books',(req,res)=>{
    response.data.push(
        "Clean JavaScript",
        "Distribuided System Node",
        "Handbook Redux"
    );
    return res.send(response);
})
//get carros api/v1/cars
app.get('/api/v1/cars',(req,res)=>{
    response.data.push(
        "Nissan",
        "Ferrari",
        "Fiat"
    );
    return res.send(response);
})


module.exports = app;