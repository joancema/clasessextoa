const express = require('express') ;
const app = express();


const response =
{
    data:[],
    services:"Book services",
    architecture:"Microservices"
}

const logger = message=> console.log(`Mensaje desde Book service:  ${message}`)

app.use((req,res,next)=>{
    response.data=[];
    next();
})

//url http://localhost:3000/api/v2/books

app.get("/api/v2/books", (req,res)=>{
    response.data.push(
        "Viaje a la luna",
        "Vuelta al mundo en ...",
        "Misterios"
    );
    logger("Get data book");
    return res.send(response);
})

module.exports= app;