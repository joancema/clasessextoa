const express = require('express') ;
const app = express();


const response =
{
    data:[],
    services:"Car services",
    architecture:"Microservices"
}

const logger = message=> console.log(`Mensaje desde Car service:  ${message}`)

app.use((req,res,next)=>{
    response.data=[];
    next();
})

//url http://localhost:3000/api/v2/cars

app.get("/api/v2/cars", (req,res)=>{
    response.data.push(
        "Hyundai",
        "Renault",
        "Volvo"
    );
    logger("Get data cars");
    return res.send(response);
})

module.exports= app;