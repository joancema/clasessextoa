const libros=[
    {
        id:1,
        titulo:'Aprendiendo javaScript',
        idautor:1
    },
    {
        id:2,
        titulo:'HTML para principiantes',
        idautor:2
    }
]
const autores=[
    {
        id:1,
        nombre:'john'
    },
    {
        id:2,
        nombre:'pedro'
    }
]


function buscarLibroPorId(id)
{
    return new Promise((resolve,reject)=>{

        
        const libro= libros.find((libro)=>
        libro.id===id
        )
        
        if (!libro)
        {
            const error= new Error();
            error.message='Libro no encontrado'
            reject(error);
        }
        resolve(libro);


    })
    
}

function buscarAutorPorId(id)
{
    return new Promise((resolve,reject)=>{

        const autor= autores.find((autor)=>
        autor.id===id
        )
        
        if (!autor)
        {
            const error= new Error();
            error.message='Libro no encontrado'
            reject(error);
        }
        resolve(autor);

        })


}

let libroAuxiliar={};

buscarLibroPorId(2).then(libro=>{
    libroAuxiliar= libro;
    return buscarAutorPorId(libro.id);
}).then(autor=>{
    //console.log(autor)
    libroAuxiliar.autor=autor;
    delete libroAuxiliar.idautor;
    console.log(libroAuxiliar);
}).catch(error=>{
    console.log(error.message)
})


