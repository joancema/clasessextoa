const libros=[
    {
        id:1,
        titulo:'Aprendiendo javaScript'
    },
    {
        id:2,
        titulo:'HTML para principiantes'
    }
]


function buscarPorId(id,callback)
{
    const libro= libros.find((libro)=>
        libro.id===id
    )
    
    if (!libro)
    {
        const error= new Error();
        error.message='Libro no encontrado'
        return callback(error);
    }
    callback(null, libro);

}

buscarPorId(12, (err,libro)=>{
    if (err)
    {
        return console.log ( err.message)
    }
    return console.log ( libro)
});
