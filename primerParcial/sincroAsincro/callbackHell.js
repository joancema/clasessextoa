const libros=[
    {
        id:1,
        titulo:'Aprendiendo javaScript',
        idautor:1
    },
    {
        id:2,
        titulo:'HTML para principiantes',
        idautor:2
    }
]
const autores=[
    {
        id:1,
        nombre:'john'
    },
    {
        id:2,
        nombre:'pedro'
    }
]


function buscarLibroPorId(id,callback)
{
    const libro= libros.find((libro)=>
        libro.id===id
    )
    
    if (!libro)
    {
        const error= new Error();
        error.message='Libro no encontrado'
        return callback(error);
    }
    callback(null, libro);

}

function buscarAutorPorId(id,callback)
{
    const autor= autores.find((autor)=>
        autor.id===id
    )
    
    if (!autor)
    {
        const error= new Error();
        error.message='Libro no encontrado'
        return callback(error);
    }
    callback(null, autor);

}



buscarLibroPorId(2, (err,libro)=>{
    if (err)
    {
        return console.log ( err.message)
    }

    buscarAutorPorId(libro.id, (err,autor)=>{
        if (err)
        {
            return console.log(err.message)
        }
        libro.autor=autor;
        delete libro.idautor;
        console.log(libro);
        //console.log(`El libro ${libro.titulo} fue escrito por ${autor.nombre}`)
    } )
});
