const libros=[
    {
        id:1,
        titulo:'Aprendiendo javaScript',
        idautor:1
    },
    {
        id:2,
        titulo:'HTML para principiantes',
        idautor:2
    }
]
const autores=[
    {
        id:1,
        nombre:'john'
    },
    {
        id:2,
        nombre:'pedro'
    }
]


async function buscarLibroPorId(id)
{
    
        
        const libro= libros.find((libro)=>libro.id===id)
        if (!libro)
        {
            const error= new Error();
            error.message='Libro no encontrado'
            throw error;
        }
        return libro;

    
}

async function buscarAutorPorId(id)
{
    

        const autor= autores.find((autor)=>
        autor.id===id
        )
        
        if (!autor)
        {
            const error= new Error();
            error.message='Libro no encontrado'
            throw error;
        }
        return autor;



}


async function main()
{
    try
    {
        const libro = await buscarLibroPorId(3);
        const autor = await buscarAutorPorId(libro.id);
        console.log(`Libro se llama ${libro.titulo} y el autor ${autor.nombre}`);    
    }
    catch(ex)
    {
        console.log(ex.message);
    }
}


main();