window.addEventListener('load',function(){
    btnconsultar.addEventListener('click',function(){
        
        fetch('http://localhost:3000')
        .then(function(response) {
            return response.json();
        })
        .then(function(myJson) {
            console.log(myJson);
            let htmlGenerado='<table>';

            myJson.forEach(element => {
                htmlGenerado+='<tr>'
                    htmlGenerado+='<td>'
                        htmlGenerado+= ` <button class="boton" value=${ element.nombre }>Consultar</button> `
                    htmlGenerado+='</td>'
                    htmlGenerado+='<td>'
                        htmlGenerado+=element.nombre
                    htmlGenerado+='</td>'
                    htmlGenerado+='<td>'
                        htmlGenerado+=element.apellido
                    htmlGenerado+='</td>'
                htmlGenerado+='</tr>'
            });
            resultado.innerHTML=htmlGenerado
            document.querySelectorAll('.boton').forEach(ele=>{
                ele.addEventListener('click',function(){
                    fetch(`http://localhost:3000/individual/${ele.value}`)
                    .then(function(response) {
                        return response.json();
                    })
                    .then(function(responsejson){
                        txtnombre.value=responsejson.nombre;
                        txtapellido.value=responsejson.apellido;
                    })

                })
            })
        });

    })
    btngrabar.addEventListener('click',function(){
        var url = 'http://localhost:3000';
        var data = {nombre: txtnombre.value  , apellido: txtapellido.value };

        fetch(url, {
        method: 'POST',
        body: JSON.stringify(data),
        headers:{
            'Content-Type': 'application/json'
        }
        })
        //.then(res=>res.text())
        //.then(response=> console.log(response))
        .then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response));
    })

    btnmodificar.addEventListener('click',function(){
        var url = 'http://localhost:3000';
        var data = {nombre: txtnombre.value  , apellido: txtapellido.value };
        fetch(url,{
            method:'PUT',
            body: JSON.stringify(data),
            headers:{
                'Content-Type':'application/json'
            }
        })
        .then(res =>{
            return res.json()
        } )
        .catch(error => 
        {
              console.error('Error:', error)
        })
        .then(response => 
        {
            console.log('Success:', response)
        });
        
        
    })

    btneliminar.addEventListener('click',function(){
        var url = 'http://localhost:3000/'+ txtnombre.value ;
        console.log(url)
        fetch(url,{
            method:'DELETE',
            headers:{
                'Content-Type':'application/json'
            }
        })
        .then(respuesta=>{
            return respuesta.json()
        })
        .then(respuestajson=>{
            console.log(respuestajson)
        })
        .catch(error=>{
            console.log(error)
        })
    })




    btnhtml.addEventListener('click',function(){
        fetch('http://localhost:3000/html')
        .then(function(response) {
            return response.text();
        }).then(function(texto){
            resultado.innerHTML= texto;


            
        })
        fetch('http://localhost:3000/script')
        .then(function(response){
            return response.text();
        }).then(function(texto){
            var script = document.createElement( "script" );
            script.type = "text/javascript";
            script.text = texto;
            document.getElementsByTagName('head')[0].appendChild(script)
        })
        
        
    })
        
})