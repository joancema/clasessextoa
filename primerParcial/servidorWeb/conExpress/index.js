const express = require('express')
const server = express();
const path = require("path");

const fs = require('fs');
const { Server } = require('http');

const home = fs.readFileSync('./index.html');
const about= fs.readFileSync('./about.html');


/*
server.get('/',(req,res)=>{
    res.write(home)
})
*/
server.get('/',getHomePage)

server.get('/about',(req,res)=>{
    res.write(about)
})

server.get('/prueba',(req,res)=>{
    let pruebax=[{dato:'hola mundo'}] 
    res.send(pruebax)
})

server.use(express.json())
server.listen(8080, ()=>{
    console.log(`Servidor corriendo en el puerto 8080`);
})

var errorPg = path.join(__dirname, "./error.html");

server.use((req,res,next)=>{
    res.status(404).sendFile(errorPg);
})


function getHomePage(req,res)
{
    return res.write(home)
}