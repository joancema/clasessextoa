const express = require('express')
var bodyParser = require('body-parser')
const app = express()
app.use(bodyParser.json({ type: 'application/json' }))
app.use('/publico',express.static(__dirname + '/publico'));
//app.use(bodyParser())
app.use(express.json())

let arreglo=[];

app.get('/', function (req, res) {
  res.send(arreglo)
})
app.get('/individual/:nombrex',function(req,res){
  res.send( arreglo.filter(p=>{ return p.nombre== req.params.nombrex  })[0] )
})
app.get('/html',function(req,res){
  let cadena=``;
  cadena+= ` <label>Usuario</label> `
  cadena+= ` <input type='text' value='' id='usuario'> `
  cadena+= ` <label>Clave</label> `
  cadena+= ` <input type='text' value='' id='clave'> `
  cadena+= ` <button id='comprobar' onClick='prueba()'>Aceptar</button> `
  
  res.send(cadena)
})

app.get('/script',function(req,res){
  let cadena=``;
  cadena+= ` function prueba() {`
  cadena+= ` alert('hola desde el servidor') `
  cadena+= `}`
  
  res.send(cadena)
})



app.post('/', function(req,res){
    arreglo.push(req.body);
    res.send({'mensaje':'elemento almacenado'})
})
app.put('/', function(req,res){
  //arreglo[ arreglo.findIndex(p=>{return p.nombre==req.body.nombre })]=req.body
  objeto= arreglo.filter(p=>{return p.nombre==req.body.nombre })[0];
  objeto.apellido= req.body.apellido ;
  res.send({'mensaje':'elemento modificado'})
})
app.delete('/:nombrex',function(req,res){
  console.log(req.params)
  arreglo= arreglo.filter(ele=>{ return ele.nombre !=req.params.nombrex})
  res.send({'mensaje':'elemento eliminado!!'})

})
 
app.listen(3000)