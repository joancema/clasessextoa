const {createContainer, asClass, asValue, asFunction} =  require('awilix');

//config

const config =  require('../config')
const app = require('.')

//servicios
const {HomeService, UserService } = require('../services')
//controladores
const {HomeController} = require('../controllers')
//rutas
const { HomeRoutes  } = require('../routes/index.routes')
const Routes = require('../routes')

//modelos

const {User} = require('../models')

//repositorios

const { UserRepository  } = require('../repositories')


const container = createContainer();

container
.register({
    app: asClass(app).singleton(),
    router:asFunction(Routes).singleton(),
    config: asValue(config)
})
.register({
    HomeService: asClass(HomeService).singleton(),
    UserService: asClass(User).singleton()
}).register({
    HomeController: asClass(HomeController.bind(HomeController)).singleton()
}).register({
    HomeRoutes: asFunction(HomeRoutes).singleton()
})
.register(
    {
        User: asValue(User)
    }
)
.register(
    {
        UserRepository: asClass(UserRepository).singleton()
    }
)

module.exports = container;
