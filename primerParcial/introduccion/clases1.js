const persona={
    nombre:'john',
    apellido:'cevallos',
    esEstudiante:true,
    getNombreCompleto(){
        return this.nombre+" "+this.apellido;
    }
}

console.log(persona.nombre)
console.log(persona['apellido']);
console.log(persona.getNombreCompleto());