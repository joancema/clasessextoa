const saludame = (nombre)=> console.log(`Hola ${nombre}`);
saludame("John Cevallos");


/*
Buena pregunta, servirá para aclarar algo que todavía no es entendido en el mundo JavaScript
 por devs que toman contacto con el lenguaje.

Las funciones flecha nacieron por el problema eterno de dentro de callbacks no se sobreescriba
el contexto sin necesidad de usar .bind o el horroroso var that = this, ya que una función de
flecha no están vinculadas a métodos y por ende, no pueden ser usadas como funciones constructoras;
es decir, para crear instancias. Este tipo de funciones fue inspirado en lenguajes funcionales.

No tienes que usar funciones flecha para todo si no necesitas el comportamiento que te ofrece.
En el caso que has expuesto, no tiene sentido usar una función flecha si se quiere tener acceso 
a this simplemente porque no lo tendrá. En este contexto entendemos que las funciones flecha no son
un reemplazo a las funciones estándar ni mucho menos; solo han venido como un enfoque diferente 
ya que no comparten las mismas características. Ambas pueden convivir en armonía.
*/